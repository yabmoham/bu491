import pandas as pd
#Load the Data
df = pd.read_csv('Insider_Data.csv', low_memory=False)

#Transaction code filtering
df = df.loc[(df['TRANCODE'] == 'P') | (df['TRANCODE'] == 'S')]
#Acquisition/Disposition filtering
df = df.loc[(df['ACQDISP'] == 'A') | (df['ACQDISP'] == 'D')]
#Cleanse code filtering
df = df.loc[(df['CLEANSE'] == 'R') | (df['CLEANSE'] == 'H') | (df['CLEANSE'] == 'L')| (df['CLEANSE'] == 'I')]

# Solve for delta amount changes
dAmount = []
for index, row in df.iterrows():
    multiplier = 1 if row['ACQDISP'] == 'A' else -1
    dAmount.append((multiplier * row['SHARES_ADJ'] * row['TPRICE_ADJ'])/1000000)
df['dAmount'] = dAmount

avgAmountTickerDF, avgFreqTickerDF = pd.DataFrame(), pd.DataFrame()
for i in range(13):
    avgAmountTickerDF[str(2010 + i)] = []
    avgFreqTickerDF[str(2010 + i)] = []

def getTickerBreakdown(df, ticker):
    years = [2010 + i for i in range(13)]
    yeardAmount = []
    frequency = []
    for year in years:
        yearDF = df.loc[df['TRANDATE'] // 10000 == year]
        numTraders = df['PERSONID'].nunique()
        frequency.append(yearDF['dAmount'].count() / numTraders if numTraders > 0 else 0)
        yeardAmount.append(yearDF['dAmount'].sum() / numTraders if numTraders > 0 else 0)
    avgAmountTickerDF.loc[len(avgAmountTickerDF)] = yeardAmount
    avgFreqTickerDF.loc[len(avgFreqTickerDF)] = frequency

for ticker in df['TICKER'].unique():
    getTickerBreakdown(df.loc[df['TICKER'] == ticker], ticker)

for i in range(2010, 2023):
    printthis = avgAmountTickerDF[str(i)].mean()
    print(str(i))
    print(printthis)


#def prettyPrint(amountDF, freqDF):
    
'''
# Helper function for getting frequency/delta amount breakdown
def getBreakdown(df):
    #Array of years
    years = [2010 + i for i in range(13)]
    yeardAmount = []
    frequency = []
    for year in years:
        yearDF = df.loc[df['TRANDATE'] // 10000 == year]
        frequency.append(yearDF['dAmount'].count())
        yeardAmount.append(yearDF['dAmount'].sum())

    consolidatedDF = pd.DataFrame()
    consolidatedDF["Year"] = years
    consolidatedDF["Freq"] = frequency
    consolidatedDF["dAmount"] = yeardAmount
    print(consolidatedDF)
    print("Total Amount Change", consolidatedDF['dAmount'].sum())
    return consolidatedDF

# We're going to write our dataframes to an excel file
with pd.ExcelWriter('output.xlsx') as writer:
    final_df = pd.DataFrame()
    final_df["Year"] = [2010 + i for i in range(13)]

    #Elon Breakdown
    print("Breakdown for Elon Musk")
    elon_df = df.loc[df['PERSONID'] == ELON_ID]
    elon_df = getBreakdown(elon_df)
    elon_df.to_excel(writer, sheet_name='Elon Musk')
    final_df['Elon Musk (dAmount)'] = elon_df['dAmount']

    # Breakdown of all insiders:
    df = df.loc[df['PERSONID'] != ELON_ID]
    print("Breakdown for all Insiders")
    insiders_df = getBreakdown(df)
    insiders_df.to_excel(writer, sheet_name='All Insiders')
    print("correlation with Elon (Freq):", insiders_df['Freq'].corr(elon_df['Freq']))
    print("correlation with Elon (dAmount):", insiders_df['dAmount'].corr(elon_df['dAmount']))
    final_df['All Insiders (dAmount)'] = insiders_df['dAmount']

    #Breakdown of individual insiders:
    for personID in df['PERSONID'].unique():
        print("Breakdown for Insider:", personID)
        personalDF = df.loc[df['PERSONID'] == personID]
        insiders_df = getBreakdown(personalDF)
        insiders_df.to_excel(writer, sheet_name=str(personID))
        print("correlation with Elon (Freq):", insiders_df['Freq'].corr(elon_df['Freq']))
        print("correlation with Elon (dAmount):", insiders_df['dAmount'].corr(elon_df['dAmount']))
        final_df[str(personID)+' (dAmount)'] = insiders_df['dAmount']
    final_df.to_excel(writer, sheet_name='Cumulative')
'''