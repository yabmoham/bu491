options ls=max ps=max nocenter nodate threads obs=max;

data rdq;
infile "C:\Users\Andriy Shkilko\Dropbox\Teaching\BU493Y\BU493y Winter 2023\exercises\rdq.csv" dsd dlm="," lrecl=1000 firstobs=2;
input gvkey	datadate fyearq fqtr indfmt $ consol $ popsrc $ datafmt $ ticker $ curcdq $ datacqtr $ datafqtr $ date $ costat $;

if ticker ne " "; if date ne "."; ea=1;
*Converting dates into SAS dates. This is SAS-specific; date=input(date,yymmdd8.);

keep ticker date ea;
proc sort data=rdq; by ticker date;

data crsp;
infile "C:\Users\Andriy Shkilko\Dropbox\Teaching\BU493Y\BU493y Winter 2023\exercises\crsp.csv" dsd dlm="," lrecl=1000 firstobs=2;
input PERMNO date $ SHRCD EXCHCD SICCD $ NCUSIP $ TICKER $ PERMCO CUSIP $ PRC VOL retx $ shout mret spret;

*Converting dates into SAS dates. This is SAS-specific; date=input(date, yymmdd8.);

*BEG. Filters for common stocks, NYSE- and Nasdaq-listed stocks, prices inferred from the closing quotes, and faulty return observations;
if shrcd=10 or shrcd=11;
if exchcd=1 or exchcd=3;
if prc lt 0 then prc=prc*-1;
if retx ne "B" and retx ne "C";
ret=retx*1;
*END. Filters for common stocks, NYSE- and Nasdaq-listed stocks, prices inferred from the closing quotes, and faulty return observations;

*Market-adjusting; ret=ret-mret;

keep date ticker ret;

proc sort data=crsp; by ticker date;

/*proc univariate data=one; var prc vol ret shout mret spret; run;*/

data crsp; merge crsp rdq; by ticker date; 
if ret ne .;

data insiders;
*length cname $ 15;
infile "C:\Users\Andriy Shkilko\Dropbox\Teaching\BU493Y\BU493y Winter 2023\exercises\jhhmfqi8zy25tajm.csv" dsd dlm="," lrecl=1000 firstobs=2;
input FDATE	CDATE DCN $ SEQNUM FORMTYPE PERSONID OWNER $ SECID TICKER $ CUSIP6 $ CUSIP2	CNAME $ ROLECODE1 $	ROLECODE2 $ ROLECODE3 $ ROLECODE4 $ 
TRANCODE $ ad $ date $ TPRICE OWNERSHIP $ SHARES SHARESHELD AMEND $ SECDATE SIGDATE MAINTDATE CLEANSE $ TRANCODE_AR $ ACQDISP_AR $ 
TPRICE_AR TRANDATE_AR OPTIONSELL $ SECTITLE $ SHARES_ADJ SHARESHELD_ADJ	TPRICE_ADJ	ADDRESS1 $ ADDRESS2 $ CITY $ STATE $ ZIPCODE $ COUNTRY $ 
PHONE $ CNUM $ SECTOR INDUSTRY;

date=input(date, yymmdd8.);

if trancode="P" or trancode="S";
if cleanse="R" or cleanse="H";
if ticker ne " ";

ins=1;

keep ticker date rolecode1 ins personid shares;

*Since the downloaded version of CRSP doesn't go past this date; if date lt 20220400;

if rolecode1="CFO";

proc sort data=insiders; by ticker date personid;
proc means data=insiders sum noprint; var shares; by ticker date personid; output out=insiders sum=shares;
proc means data=insiders n sum noprint; var personid shares; by ticker date; output out=insiders (drop=_type_ _freq_) n=nins a sum=b shares;


data combine; merge crsp insiders; by ticker date; 
*Comment out for the #insiders; *nins=shares; 
if nins=. then nins=0;

*BEG. Building the pre-event window;
proc sort data=combine; by ticker descending date;
data pre; set combine;

retain nn .;
lticker=lag(ticker);
if ea ne . then n=0; else if ea=. then n+1; if ticker ne lticker then nn=.; if ea ne . then nn=n; if nn ne .;
if n le 30; m=n*-1; if m ne .;

proc sort data=pre; by m ticker;

*If the following line is commented out, we are averaging across insider trades without prior averaging across insider trades in a particular stock;
proc means data=pre mean noprint; var nins; by m ticker; output out=pre mean=nins;
proc means data=pre mean noprint; var nins; by m; output out=pre mean=nins;
*END. Building the pre-event window;

*BEG. Building the post-event window;
proc sort data=combine; by ticker date;
data post; set combine;

retain nn .;
lticker=lag(ticker);
if ea ne . then m=0; else if ea=. then m+1; if ticker ne lticker then nn=.; if ea ne . then nn=m; if nn ne .;
if m le 30; if m ne .;

proc sort data=post; by m ticker;

proc means data=post mean noprint; var nins; by m ticker; output out=post mean=nins;
proc means data=post mean noprint; var nins; by m; output out=post mean=nins; run;
*END. Building the post-event window;

data combin; set pre post; proc sort data=combin; by m;

proc print data=combin (obs=500) noobs; run;
