# %%
#Importing Libraries
import pandas as pd
import numpy as np
import plotly_express as px
from pandas.tseries.offsets import BDay
from datetime import timedelta

# %%
#Importing Insider Data 

path = "Insider_Data.csv"
df_insiders = pd.read_csv(path,encoding="unicode_escape")

# %%
#Cleaning Insider Data
df_filter = df_insiders[["DCN","SEQNUM",'PERSONID', 
'OWNER','TICKER','ROLECODE1','ACQDISP','TRANDATE','CLEANSE','SHARES_ADJ', 
'TPRICE_ADJ']]
df_filter = df_filter.loc[df_filter["CLEANSE"].isin(["R","H"])]
df_filter = df_filter.dropna(subset=["TPRICE_ADJ","SHARES_ADJ"])
df_acc = df_filter.loc[df_filter["ACQDISP"]=="A"]
df_sale = df_filter.loc[df_filter["ACQDISP"]=="D"]
df_sale.TPRICE_ADJ = df_sale.TPRICE_ADJ.apply(lambda x: -1*x)
df_filter = pd.concat([df_acc,df_sale])
df_filter["TAMOUNT"] = (df_filter["TPRICE_ADJ"].astype(int)*df_filter["SHARES_ADJ"].astype(int))/1000000
df_filter["DATE VAL"] = pd.to_datetime(df_filter["TRANDATE"])
df_filter["tic"]  = df_filter["TICKER"]

# %%
#Importing Earnings Data
path2 = "earnings_call.csv"
df_earnings = pd.read_csv(path2,engine="pyarrow")

# %%
#Cleaning Earnings Data
df_earn_filter = df_earnings[["datacqtr","rdq","tic"]]
df_earn_filter["year"] = df_earn_filter["datacqtr"].apply(lambda x: x[:-2])
df_earn_filter["quarter"] = df_earn_filter["datacqtr"].apply(lambda x: x[-2:])
df_earn_filter["rdq"] = pd.to_datetime(df_earn_filter["rdq"])
df_earn_filter = df_earn_filter.dropna(subset=["rdq"])
df_earn_filter = df_earn_filter.drop_duplicates(keep = "first",subset=['rdq',"tic","datacqtr"])
df_earn_filter["rdq2"] = df_earn_filter["rdq"]
df_earn_filter = df_earn_filter.reset_index(drop = True)

# %%
#Adding +30 -30 around Earning Calls
df_earn_filter["DATE VAL"] = df_earn_filter.apply(lambda row: pd.date_range(start=row['rdq']-BDay(30),end = row['rdq']+BDay(30),freq="B"),axis = 1)
df_earn_filter["DATE VAL"] = df_earn_filter.apply(lambda row: pd.date_range(start=row['rdq']-timedelta(30),end = row['rdq']+timedelta(30)),axis = 1)
df_window = df_earn_filter.explode("DATE VAL")
df_window["days"] = df_window.groupby(by = ['rdq',"tic","datacqtr"]).cumcount()
df_window["days"] = df_window["days"] - 30

# %%
#Inner Merging pn ticker and DATE VAL
df_merged = pd.merge(df_window,df_filter, how="inner",on = ["tic","DATE VAL"])

# %%
#Grouping by Days
dearnings_group = df_merged.groupby(["days","tic","PERSONID"]).agg({"OWNER":"count"})
dearnings_group = dearnings_group.groupby(["days","tic"]).agg({"OWNER":np.mean})
dearnings_group = dearnings_group.groupby(["days"]).agg({"OWNER":np.mean})
dearnings_group["FREQ"] = dearnings_group["OWNER"]
dearnings_group = pd.DataFrame(dearnings_group)
dearnings_group = (dearnings_group).reset_index()

# %%
#"FREQ OF INSIDER TRADES AFTER QUARTERLY EARNING CALLS [-30,+30]
# %%
fig1 = px.line(dearnings_group,x = "days", y = "FREQ",title="AVERAGE FREQ OF INSIDER TRADES PER INSIDER AFTER QUARTERLY EARNING CALLS [-30,+30]")
fig1.add_vline(x=0)
fig1.show()
