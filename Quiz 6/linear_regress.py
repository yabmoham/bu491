import pandas as pd
import numpy as np
import statsmodels.api as sm

# CEOs are opportunistic traders and CFOs are more risk averse, is a CEO trading a stronger signal than a CFO trading?

insider_rolecodes = ['CEO', 'CFO']
t_threshold = 10000
# Read the data
crsp = pd.read_csv("crspq68.csv")
insider = pd.read_csv("insiders68.csv")
merged = pd.merge(crsp, insider, on=['ticker', 'date'], how='inner')
merged['adjret'] = merged['ret'] - merged['mret']
merged['mcap'] = merged['mcap'] * 1000000
merged['adjret'] = merged['adjret'] * 100
insider_CEOs = merged.loc[merged["rolecode"].isin(insider_rolecodes)]

insider_CEOs['ceo_buy'] = np.where((insider_CEOs['rolecode'] == 'CEO') & (insider_CEOs['ad'] == 'A'), 1, 0)
insider_CEOs['ceo_sell'] = np.where((insider_CEOs['rolecode'] == 'CEO') & (insider_CEOs['ad'] == 'D'), 1, 0)
insider_CEOs['cfo_buy'] = np.where((insider_CEOs['rolecode'] == 'CFO') & (insider_CEOs['ad'] == 'A'), 1, 0)
insider_CEOs['cfo_sell'] = np.where((insider_CEOs['rolecode'] == 'CFO') & (insider_CEOs['ad'] == 'D'), 1, 0)

x = insider_CEOs[['ceo_buy', 'ceo_sell', 'cfo_buy', 'cfo_sell', 'mcap', 'tsize']]
y = insider_CEOs['adjret']
X = sm.add_constant(x)
model = sm.OLS(y,X.astype(float)).fit()
print(model.summary())




# dfRetRegs = df_merged_trends[["TICKER","YEAR","MONTH","RET","BM","MKTCAP","TREND_50","TREND_75","TREND_95"]]
# dfRetRegs['RET_t1'] = dfRetRegs.groupby("TICKER")["RET"].shift(1)
# dfRetRegs = dfRetRegs.loc[dfRetRegs["RET"] != "C"]
# dfRetRegs = dfRetRegs.loc[dfRetRegs["RET_t1"] != "C"]
# dfRetRegs["RET"] = pd.to_numeric(dfRetRegs["RET"])
# dfRetRegs["RET_t1"] = pd.to_numeric(dfRetRegs["RET_t1"])
# result = dfRetRegs.groupby(["TICKER","YEAR"])["RET"].apply(lambda x: x.add(1).cumprod().sub(1)).reset_index()
# dfRetRegs["Year_RET"] = result["RET"]
# dfRetRegs["RET"] = 100 * dfRetRegs["RET"]
# dfRetRegs["RET_t1"] = dfRetRegs["RET_t1"] * 100
# dfRetRegs['MKTCAP'] = np.log(dfRetRegs['MKTCAP'])
# dfRetRegs['BM'] = np.log(dfRetRegs['BM'])
# dfRetRegs.replace([np.inf, -np.inf], np.nan, inplace=True)
# dfRetRegs = dfRetRegs.dropna()
