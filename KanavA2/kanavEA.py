# %%
#Importing Libraries
import pandas as pd
import matplotlib.pyplot as py
import numpy as np
import plotly_express as px
from datetime import datetime
from datetime import timedelta
from pandas.tseries.offsets import BDay

# %%
#Importing Insider Data 

path = "tr_insiders_22y.csv"
df_insiders = pd.read_csv(path,encoding="unicode_escape")

# %%
#Cleaning Insider Data
df_filter = df_insiders[["DCN","SEQNUM",'PERSONID', 
'OWNER','TICKER','ROLECODE1','ACQDISP','TRANDATE','CLEANSE','SHARES_ADJ', 
'TPRICE_ADJ']]
df_filter = df_filter.loc[df_filter["CLEANSE"].isin(["R","H"])]
df_filter = df_filter.dropna(subset=["TPRICE_ADJ","SHARES_ADJ"])
df_acc = df_filter.loc[df_filter["ACQDISP"]=="A"]
df_sale = df_filter.loc[df_filter["ACQDISP"]=="D"]
df_sale.TPRICE_ADJ = df_sale.TPRICE_ADJ.apply(lambda x: -1*x)
df_filter = pd.concat([df_acc,df_sale])
df_filter["TAMOUNT"] = 
(df_filter["TPRICE_ADJ"].astype(int)*df_filter["SHARES_ADJ"].astype(int))/1000000
df_filter["DATE VAL"] = pd.to_datetime(df_filter["TRANDATE"])
df_filter["tic"]  = df_filter["TICKER"]
# df_filter = df_filter.sort_values(by = ["SHARES_ADJ"],ascending=False)
# df_filter = df_filter.drop_duplicates(keep = "first",subset=["date","TICKER"])
# df_acc = df_filter.loc[df_filter["ACQDISP"]=="A"]
# df_sale = df_filter.loc[df_filter["ACQDISP"]=="D"]

# %%
#Importing Earnings Data
path2 = "earnings_call.csv"
df_earnings = pd.read_csv(path2,engine="pyarrow")

# %%
#Cleaning Earnings Data
df_earn_filter = df_earnings[["datacqtr","rdq","tic"]]
df_earn_filter["year"] = df_earn_filter["datacqtr"].apply(lambda x: x[:-2])
df_earn_filter["quarter"] = df_earn_filter["datacqtr"].apply(lambda x: x[-2:])
df_earn_filter["rdq"] = pd.to_datetime(df_earn_filter["rdq"])
df_earn_filter = df_earn_filter.dropna(subset=["rdq"])
df_earn_filter = df_earn_filter.drop_duplicates(keep = 
"first",subset=['rdq',"tic","datacqtr"])
df_earn_filter["rdq2"] = df_earn_filter["rdq"]
df_earn_filter = df_earn_filter.reset_index(drop = True)

# %%
#Adding +30 -30 around Earning Calls
df_earn_filter["DATE VAL"] = df_earn_filter.apply(lambda row: 
pd.date_range(start=row['rdq']-BDay(30),end = row['rdq']+BDay(30),freq="B"),axis = 1)
# df_earn_filter["DATE VAL"] = df_earn_filter.apply(lambda row: 
pd.date_range(start=row['rdq']-timedelta(30),end = row['rdq']+timedelta(30)),axis = 1)
df_window = df_earn_filter.explode("DATE VAL")
df_window["days"] = df_window.groupby(by = ['rdq',"tic","datacqtr"]).cumcount()
df_window["days"] = df_window["days"] - 30

# %%
#Inner Merging pn ticker and DATE VAL
df_merged = pd.merge(df_window,df_filter, how="inner",on = ["tic","DATE VAL"])

# %%
#Grouping by Days
dearnings_group = df_merged.groupby(["days","tic","PERSONID"]).agg({"OWNER":"count"})
dearnings_group = dearnings_group.groupby(["days","tic"]).agg({"OWNER":np.mean})
dearnings_group = dearnings_group.groupby(["days"]).agg({"OWNER":np.mean})
dearnings_group["FREQ"] = dearnings_group["OWNER"]
dearnings_group = pd.DataFrame(dearnings_group)
dearnings_group = (dearnings_group).reset_index()

# %%
#"FREQ OF INSIDER TRADES AFTER QUARTERLY EARNING CALLS [-30,+30]
# %%
fig1 = px.line(dearnings_group,x = "days", y = "FREQ",title="AVERAGE FREQ OF INSIDER 
TRADES PER INSIDER AFTER QUARTERLY EARNING CALLS [-30,+30]")
fig1.add_vline(x=0)
fig1.show()

############# Extra Analysis after this line ##################


# %%
#Grouping by Purchases and Sales
dearnings_group_ps = 
df_merged.groupby(["ACQDISP","days","tic","PERSONID"]).agg({"OWNER":"count"})
dearnings_group_ps = 
dearnings_group_ps.groupby(["ACQDISP","days","tic"]).agg({"OWNER":np.mean})
dearnings_group_ps = 
dearnings_group_ps.groupby(["ACQDISP","days"]).agg({"OWNER":np.mean})
dearnings_group_ps["FREQ"] = dearnings_group_ps["OWNER"]
dearnings_group_ps = pd.DataFrame(dearnings_group_ps)
dearnings_group_ps = dearnings_group_ps.reset_index()

# %%
#"FREQ OF INSIDER TRADES AFTER QUARTERLY EARNING CALLS [-30,+30] by Purchases and 
Sales"
# %%
fig2 = px.line(dearnings_group_ps,x = "days", y = "FREQ",color="ACQDISP",title="FREQ 
OF INSIDER TRADES PER INSIDER AFTER QUARTERLY EARNING CALLS [-30,+30] by Purchases and 
Sales")
fig2.add_vline(x=0)
fig2.show()

# %%
#Grouping by CEO and CFO
df_merged_role = df_merged.loc[df_merged["ROLECODE1"].isin(["CFO","CEO"])]
dearnings_group_role = 
df_merged_role.groupby(["ROLECODE1","days","tic","PERSONID"]).agg({"OWNER":"count"})
dearnings_group_role = 
dearnings_group_role.groupby(["ROLECODE1","days","tic"]).agg({"OWNER":np.mean})
dearnings_group_role = 
dearnings_group_role.groupby(["ROLECODE1","days"]).agg({"OWNER":np.mean})
dearnings_group_role["FREQ"] = dearnings_group_role["OWNER"]
dearnings_group_role = pd.DataFrame(dearnings_group_role)
dearnings_group_role = dearnings_group_role.reset_index()

# %%
#"FREQ OF INSIDER TRADES AFTER QUARTERLY EARNING CALLS [-30,+30] by CEO's and CFO's"
# %%
fig3 = px.line(dearnings_group_role,x = "days", y = 
"FREQ",color="ROLECODE1",title="FREQ OF INSIDER TRADES PER INSIDER AFTER QUARTERLY 
EARNING CALLS [-30,+30] by CEO's and CFO's")
fig3.add_vline(x=0)
fig3.show()

# %%
#Importing 10B5 Data
path3 = "10b5.csv"
df_routine = pd.read_csv(path3,engine="pyarrow")

# %%
#Creating Oppurtunistic Insider Trade Data
df_routine = df_routine.loc[df_routine["TABLE_TYPE"]== 1]
df_flag = pd.merge(df_filter,df_routine,how="left",on = ["DCN","SEQNUM"])
df_flag["FLAG"] = df_flag["FLAG"].astype(str)
df_flag = df_flag.loc[~(df_flag["FLAG"]=="Y")]

# %%
df_merged_opp = pd.merge(df_window,df_flag, how="inner",on = ["tic","DATE VAL"])

# %%
#Grouping by Days
dearnings_group_10b5 = 
df_merged_opp.groupby(["days","tic","PERSONID"]).agg({"OWNER":"count"})
dearnings_group_10b5 = 
dearnings_group_10b5.groupby(["days","tic"]).agg({"OWNER":np.mean})
dearnings_group_10b5 = dearnings_group_10b5.groupby(["days"]).agg({"OWNER":np.mean})
dearnings_group_10b5["FREQ"] = dearnings_group_10b5["OWNER"]
dearnings_group_10b5 = pd.DataFrame(dearnings_group_10b5)
dearnings_group_10b5 = (dearnings_group_10b5).reset_index()

# %%
# %%
fig5 = px.line(dearnings_group_10b5,x = "days", y = "FREQ",title="(10B5 Excluded) 
AVERAGE FREQ OF INSIDER TRADES PER INSIDER AFTER QUARTERLY EARNING CALLS [-30,+30]")
fig5.add_vline(x=0)
fig5.show()
 


